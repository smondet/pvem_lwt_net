(** [Pvem_lwt_unix] is a library-module providing TCP/SSL client and servers. *)

open Pvem_lwt_unix
open Deferred_result
open Printf
let (|>) f x = x f


(**  This module allows to inspect the behavior of the library from outside. *)
module Inspection = struct

  let make_unconsistent_server_ssl_context = ref false
  (** Set this to [true] to create a wrong SSL context for the server,
      {!Ssl.accept} should fail then. *)

  let debug = ref false
  (** Set [debug] to [true] in order to print many debug statements to [stderr]
      (debug statements use [Printf.eprintf "PLNDBG{{%s}}\n%!"], they are independent of
      Lwt's scheduling). *)
end
module Internal = struct

  let error_net e = `Net e
  let wrap_deferred_net f =
    wrap_deferred ~on_exn:(fun e -> error_net (`Exn e)) (fun () -> f ())
  let fail_net e = fail (error_net e)

  let error_tls e = `Net (`TLS e)
  let fail_tls e = fail (error_tls e)
  let error_openssl e = error_tls (`Openssl_exn (e, Ssl.get_error_string ()))
  let fail_openssl e = fail (error_openssl e)
  let wrap_deferred_tls f =
    wrap_deferred ~on_exn:(fun e -> error_openssl e) (fun () -> f ())


  module List = ListLabels
  
  module Debug = struct
    let exn = Printexc.to_string
    let state t =
      match Lwt.state t with
      | Lwt.Return () -> "return"
      | Lwt.Fail Lwt.Canceled -> "cancelled"
      | Lwt.Fail e -> sprintf "(fail %s)" (exn e)
      | Lwt.Sleep -> "sleeping"

    let say fmt =
      ksprintf (fun s -> eprintf "PLNDBG{{%s}}\n%!" s) fmt

    let p f = 
      if !Inspection.debug then say "%s" (f ()) else ()

    let s str = str
    let f = sprintf
    let (%) = (^)
    let (@) f x = f x
  end

  module String = struct
    include StringLabels

    let sub_exn s ~index ~length = sub s index length
    let sub s ~index ~length =
      try Some (sub_exn s index length) with _ -> None

    let index_of_character t ~from c =
      try Some (index_from t from c) with _ -> None

    let split t ~on =
      let length_of_t = length t in
      let `Character char = on in
      let rec loop acc from =
        match index_of_character t ~from char with
        | Some index ->
          loop (sub_exn t ~index:from ~length:(index - from) :: acc) 
            (index + 1)
        | None ->
          (sub_exn t ~index:from ~length:(length_of_t - from) :: acc)
      in
      List.rev (loop [] 0)
  end
end

open Internal

(** The [Error] module provides [Error.to_string]. *)
module Error = struct

  let to_string = 
    let errf fmt = ksprintf (sprintf "Net: %s") fmt in
    let exn = Printexc.to_string in
    function
    | `Net (`No_host_entry s) -> errf "No host entry for %S" s
    | `Net (`Exn e) ->
      errf "Exception %s" (exn e)
    | `Net (`Socket_creation_exn e) ->
      errf "Exception while creating socket: %s" (exn e)
    | `Net (`Socket_creation_while_address_in_use (addr, port)) ->
      errf "Cannot create socket, \"%s:%d\" is already in use" addr port
    | `Net (`TLS (`Openssl_exn (e, msg))) ->
      errf "TLS OpenSSL Error %s (%S)" (exn e) msg
    | `Net (`TLS (`Context_exn e)) ->
      errf "TLS-Context Error %s" (exn e)
    | `Net (`TLS (`Not_a_tls_socket t)) ->
      errf "TLS Error NOT a TLS socket"
    | `Net (`TLS (`Certificate_cn_does_not_match (cn, host))) ->
      errf "TLS Error CN does not match: %S Vs %S" cn host
    | `Net (`TLS (`Not_implemented s)) -> errf "TLS: %S: Not Implemented" s
    | `Net (`TLS (`Wrong_subject_format s)) ->
      errf "TLS-certificate: wrong subjec format: %S" s
    | `Net (`TLS (`No_common_name_in_certificate s)) ->
      errf "TLS-certificate: no common-name in %S" s

end

module Transport_layer_security = struct

  type t = Lwt_ssl.socket

  let init () =
    Ssl.init ~thread_safe:true () 

  let connect socket ssl_context =
    wrap_deferred_tls (fun () -> Lwt_ssl.ssl_connect socket ssl_context)

  let accept socket context =
    wrap_deferred_tls (fun () -> Lwt_ssl.ssl_accept socket context)

  let shutdown socket =
    begin match Lwt_ssl.ssl_socket socket with
    | Some _ ->
      wrap_deferred_tls (fun () -> Lwt_ssl.ssl_shutdown socket)
      >>= fun () ->
      wrap_deferred_tls (fun () ->
        Lwt_ssl.shutdown socket Lwt_unix.SHUTDOWN_ALL;
        Lwt.return ())
    | None -> return ()
    end
    >>= fun () ->
    wrap_deferred_tls (fun () -> Lwt_ssl.close socket)

  let ssl_version protocol_version =
    match protocol_version with
    | `SSLv23 -> Ssl.SSLv23
    | `TLSv1 -> Ssl.TLSv1

  let basic_client_context_exn ?(protocol_version=`SSLv23) () =
    let version = ssl_version protocol_version in
    (* dbg "create_context"; *)
    let c = Ssl.create_context version Ssl.Client_context in
    c

  let server_context ?(protocol_version=`SSLv23) (cert_file, key_file) =
    let open Ssl in
    try
      let version = ssl_version protocol_version in
      let c =
        (* Here we can provoke a voluntary error for testing purposes: *)
        if !Inspection.make_unconsistent_server_ssl_context
        then Ssl.create_context version Ssl.Client_context
        else Ssl.create_context version Ssl.Server_context
      in
      use_certificate c cert_file key_file;
      (* set_cipher_list c "TLSv1"; *)
      (*
      Option.iter ca_certificate (fun ca_cert ->
        set_verify c [ Verify_peer; ] None;
        set_verify_depth c 99;
        load_verify_locations c ca_cert "";
      );
      *)
      return c
    with e -> 
      Debug.(p (fun () ->
          s "exn in server_context: openssl: " % s (Ssl.get_error_string ())
        ));
      fail_tls (`Context_exn e)

  let add_certificate_exn c ~cert ~key =
    Ssl.use_certificate c cert key

  let unsecure_client_context ?protocol_version kind =
    try
      let c = basic_client_context_exn ?protocol_version () in
      begin match kind with
      | `Anonymous -> ()
      | `With_certificate (cert, key) ->
        add_certificate_exn c ~cert ~key
      end;
      return c
    with e -> fail_tls (`Context_exn e)

  let basic_client_context 
      ?(verification_depth=99)
      ~verification_locations
      ?protocol_version
      kind =
    unsecure_client_context ?protocol_version kind
    >>= fun c ->
    begin try
      Ssl.set_verify_depth c verification_depth;
      Ssl.set_verify c [Ssl.Verify_fail_if_no_peer_cert] 
        None;
        (* (Some Ssl.client_verify_callback); *)
      begin match verification_locations with
      | `Directory dir ->
        Ssl.load_verify_locations c "" dir
      | `File file ->
        Ssl.load_verify_locations c file ""
      | `File_and_directory (file, dir) ->
        Ssl.load_verify_locations c file dir
      end;
      return c
    with e -> fail_tls (`Context_exn e)
    end

  let check_certificate ~for_hostname t =
    begin match Lwt_ssl.ssl_socket t with
    | None -> fail_tls (`Not_a_tls_socket t)
    | Some socket ->
      let cert = Ssl.get_certificate socket in
      let subject = Ssl.get_subject cert in
      begin try
        String.split subject ~on:(`Character '/')
        |> List.filter ~f:((<>) "")
        |> List.map ~f:(fun key_value ->
          match String.split key_value ~on:(`Character '=') with
          | [key;value] -> (key, value)
          | other -> failwith "KV" 
        ) |> return
      with Failure "KV" -> fail_tls (`Wrong_subject_format subject)
      end
      >>= fun fields ->
      begin try 
        let (_, cn) = List.find ~f:(fun (k, _) -> k = "CN") fields in
        Debug.(p@fun () -> f "Common Name: %S" cn);
        let condition cond =
          if cond then return () 
          else fail_tls (`Certificate_cn_does_not_match (cn, for_hostname))
        in
        let check_suffix suffix =
          let length = String.length suffix in
          let index = max 0 (String.length for_hostname - length) in
          String.sub for_hostname ~index ~length = Some suffix
          || String.sub ("." ^ for_hostname) ~index ~length = Some suffix
        in
        let check_prefix prefix =
          let length = String.length prefix in
          String.sub for_hostname ~index:0 ~length = Some prefix
          || String.sub (for_hostname ^ ".") ~index:0 ~length = Some prefix
        in
        begin match String.split cn ~on:(`Character '*') with
        | [one] -> 
          Debug.(p@fun () -> f  "Comparing %S and %S" one for_hostname);
          condition (one = for_hostname)
        | [""; suffix] -> condition (check_suffix suffix)
        | [prefix; suffix] ->
          condition (check_prefix prefix && check_suffix suffix)
        | other ->
          fail_tls (`Not_implemented "Common name too complex")
        end
      with
      | Not_found -> fail_tls (`No_common_name_in_certificate subject)
      end
    end

end

module Connection = struct
  type t = {
    inchan: Lwt_io.input_channel;
    outchan: Lwt_io.output_channel;
    socket: Transport_layer_security.t;
  }
  module TLS = Transport_layer_security

  let create inchan outchan socket =
    {inchan; outchan; socket}

  let of_tls_socket socket_fd =
    let inchan = Lwt_ssl.in_channel_of_descr socket_fd in
    let outchan = Lwt_ssl.out_channel_of_descr socket_fd in
    (create inchan outchan socket_fd)

  let in_channel t = t.inchan
  let out_channel t = t.outchan
  let shutdown {socket; inchan; outchan} =
    Debug.(p@fun () -> f "connection tls-shutdown");
    TLS.shutdown socket >>= fun () ->
    return ()

end

module type ADDRESS = sig

  type t

  val resolve_one: string ->
    (t, [> `Net of [> `Exn of exn | `No_host_entry of string ] ]) Deferred_result.t

  val resolve_all: string ->
    (t list, [> `Net of [> `Exn of exn ] ]) Deferred_result.t

  val to_string: t -> string

  val hostname: t -> string
  val inet: t -> Lwt_unix.inet_addr
end
module Address: ADDRESS = struct

  type t = {
    inet: Lwt_unix.inet_addr;
    hostname: string;
  }
  let create ~hostname inet = {inet; hostname}

  let resolve_to_array string =
    begin
      wrap_deferred_net (fun () -> Lwt_unix.gethostbyname string)
      >>< function
      | `Ok host_entry ->
        return  host_entry.Lwt_unix.h_addr_list
      | `Error (`Net (`Exn Not_found)) -> return [| |]
      | `Error other -> fail other
    end


  let resolve_one string =
    resolve_to_array string
    >>= fun array ->
    begin match array with
    | [| |] -> fail (`Net (`No_host_entry string))
    | more ->
      let pick = Random.int (Array.length more) in
      return more.(pick)
    end
    >>| create ~hostname:string

  let resolve_all string =
    resolve_to_array string
    >>| Array.map (create ~hostname:string)
    >>| Array.to_list

  let to_string t =
    sprintf "Unix: %S, Host: %S" (Unix.string_of_inet_addr t.inet) t.hostname

  let hostname t = t.hostname
  let inet t = t.inet

end

module type SOCKET_OPTIONS = sig

  type t = [
    | `Reuse_address 
    | `keep_alive
    | `Receiving_buffer of int | `Receiving_timeout of float 
    | `Sending_buffer of int | `Sending_timeout of float
  ]
  val server_default : t list
  val client_default : t list
  val set: fd:Lwt_unix.file_descr -> t list -> unit

end
module Socket_options : SOCKET_OPTIONS = struct

  type t = [
    | `Reuse_address 
    | `keep_alive
    | `Receiving_buffer of int | `Receiving_timeout of float 
    | `Sending_buffer of int | `Sending_timeout of float
  ]
  let server_default : t list = [ `Reuse_address ]
  let client_default : t list = []

  let set ~fd (socket_options : t list) =
    let module LUx = Lwt_unix in
    List.iter socket_options ~f:(function
    | `Reuse_address -> LUx.setsockopt fd LUx.SO_REUSEADDR true
    | `keep_alive -> LUx.setsockopt fd LUx.SO_KEEPALIVE true
    | `Receiving_buffer i -> LUx.setsockopt_int fd LUx.SO_RCVBUF i
    | `Receiving_timeout x -> LUx.setsockopt_float fd LUx.SO_RCVTIMEO x
    | `Sending_buffer i -> LUx.setsockopt_int fd LUx.SO_RCVBUF i
    | `Sending_timeout x -> LUx.setsockopt_float fd LUx.SO_RCVTIMEO x
    );
    ()

end

module Client  = struct

  module TLS = Transport_layer_security

  let unix_connect ?(socket_options=Socket_options.client_default) addr port =
    let inetaddr = Address.inet addr in
    let socket =
      Lwt_unix.(
        try
          let fd = socket PF_INET SOCK_STREAM 0 in
          Socket_options.set ~fd socket_options;
          fd
        with
        | Unix.Unix_error (e, s, a) as ex ->
          eprintf "Unix.Unix_error: %s %s %s\n%!" (Unix.error_message e) s a;
          raise ex
      ) in
    let sockaddr = Lwt_unix.ADDR_INET (inetaddr, port) in
    wrap_deferred_net (fun () -> Lwt_unix.connect socket sockaddr)
    >>= fun () ->
    return socket


  let connect_tcp ?socket_options ~address ~port () =
    unix_connect ?socket_options address port >>= fun unix_socket_fd ->
    let socket_fd = Lwt_ssl.plain unix_socket_fd in
    return (Connection.of_tls_socket socket_fd)

  let connect_unsecure_tls ?socket_options ?protocol_version ~address ~port connection_type =
    unix_connect ?socket_options address port >>= fun unix_socket_fd ->
    TLS.unsecure_client_context ?protocol_version connection_type
    >>= fun tls_context ->
    TLS.connect unix_socket_fd tls_context >>= fun socket_fd ->
    return (Connection.of_tls_socket socket_fd)

  let connect_basic_tls
      ?socket_options ?verification_depth 
      ~verification_locations
      ?common_name
      ?protocol_version ~address ~port connection_type =
    unix_connect ?socket_options address port >>= fun unix_socket_fd ->
    TLS.basic_client_context ?verification_depth 
      ~verification_locations ?protocol_version connection_type
    >>= fun tls_context ->
    TLS.connect unix_socket_fd tls_context >>= fun socket_fd ->
    let for_hostname =
      match common_name with
      | None -> Address.hostname address
      | Some cn -> cn in
    TLS.check_certificate ~for_hostname socket_fd
    >>= fun () ->
    return (Connection.of_tls_socket socket_fd)


end

(* module Light = struct *)
(*   (1* This should go to Pvem_lwt_unix *1) *)

(*   type t = { *)
(*     mutable lwt_t: unit Lwt.t; *)
(*     mutable lwt_u: unit Lwt.u; *)
(*     mutable awake: bool; *)
(*   } *)
(*   let create () = *)
(*     let lwt_t, lwt_u = Lwt.task () in *)
(*     {lwt_u; lwt_t; awake = false} *)

(*   let wait w = *)
(*     match w.awake with *)
(*     | true -> return () *)
(*     | false -> *)
(*       begin match Lwt.state w.lwt_t with *)
(*       | Lwt.Sleep -> () *)
(*       | Lwt.Return () | Lwt.Fail _ -> *)
(*         let t, u = Lwt.task () in *)
(*         w.lwt_t <- t; *)
(*         w.lwt_u <- u; *)
(*       end; *)
(*       wrap_deferred ~on_exn:(fun e -> e) (fun () -> w.lwt_t) *)
(*       >>< function *)
(*       | `Error Lwt.Canceled -> return () *)
(*       | `Error other -> failwith "BUG: THIS SHOULD NOT HAPPEN" *)
(*       | `Ok () -> return () *)

(*   let wake_up t = *)
(*     t.awake <- true; *)
(*     Lwt.wakeup_exn t.lwt_u Lwt.Canceled *)
(*   (1* We use Lwt.Canceled so that can re-wake-up sleepers at will *)
(*     see https://github.com/ocsigen/lwt/blob/master/src/core/lwt.ml#L312 *)
(*     where Lwt.Canceled is ignored *1) *)

(* end *)

module type SERVER = sig

  type listen_address =
    [ `Address of Address.t | `Any | `Inet of Lwt_unix.inet_addr]

  type tcp_server_error = 
    [ `Socket_creation_exn of exn
    | `Socket_creation_while_address_in_use of string * int ]

  val plain:
    ?socket_options:Socket_options.t list ->
    ?accept_number:int ->
    ?listen_on:listen_address ->
    ?stop_on:Light.t ->
    port:int ->
    ([ `Accept_exn of exn | `Connection of Connection.t] -> (unit, unit) Deferred_result.t) ->
    (Light.t, [> `Net of [> tcp_server_error ] ]) Deferred_result.t
   (** 
     The call
     [plain ~accept_number ~listen_on ~stop_on ~port handler >>= fun wait_for_end -> ...]
     creates a plain TCP server.

     - [accept_number] is the maximal number of requests to accept at once
     (c.f. {!Lwt_unix.accept_n}, default 10).
     - [listen_on] is the address to listen-on, the default being [`Any].
     - [stop_on] if provided, is a Lwt-thread that signals termination to the
     server (See [Lwt.task]: one can create a sleeping thread together with a
     “wakener”).
     - [port] is the port number to listen to.
     - [handler] is function called on every connection and on errors of
     [Lwt_unix.accept_n]. If the [handler] returns [`Error ()] the server will
     stop (as soon as possible).
     - [wait_for_end] is a lwt-thread that will be waken-up when the server
     ends.
   *)

  val tls :
    ?socket_options:Socket_options.t list ->
    ?accept_number:int ->
    ?ssl_protocol_version:[ `SSLv23 | `TLSv1 ] ->
    cert_key:string * string ->
    ?listen_on:listen_address ->
    ?stop_on:Light.t ->
    port:int ->
    ([ `Accept_exn of exn | `Connection of Connection.t | `Openssl_exn of exn * string ] -> (unit, unit) Deferred_result.t) ->
    (Light.t, [> `Net of [> tcp_server_error | `TLS of [> `Context_exn of exn ] ] ]) Deferred_result.t

end
module Server : SERVER = struct

  type listen_address =
    [ `Address of Address.t | `Any | `Inet of Lwt_unix.inet_addr]

  type tcp_server_error = 
    [ `Socket_creation_exn of exn
    | `Socket_creation_while_address_in_use of string * int ]

  module TLS = Transport_layer_security

  let server_socket 
      ?(socket_options=Socket_options.server_default) ?(listen_on=`Any) port =
    let open Lwt_unix in
    begin try
      let fd = socket PF_INET SOCK_STREAM 6 in
      let addr =
        match listen_on with
        | `Any -> Unix.inet_addr_any
        | `Inet i -> i
        | `Address a -> Address.inet a
      in
      Socket_options.set fd socket_options;
      bind fd (ADDR_INET (addr, port));
      listen fd port;
      return fd
    with
    | Unix.Unix_error (Unix.EADDRINUSE, _, _) ->
      let addr_str =
        match listen_on with
        | `Any -> "Any"
        | `Inet i -> sprintf "Inet:%s" (Unix.string_of_inet_addr i)
        | `Address a -> Address.to_string a in
      fail_net (`Socket_creation_while_address_in_use (addr_str, port))
    | e -> fail_net (`Socket_creation_exn e)
    end

  let generic
      ?socket_options ?ssl_protocol_version
      ?with_tls ?(accept_number=10)
      ?listen_on ?stop_on ~port handler =
    server_socket ?socket_options ?listen_on port >>= fun socket ->
    begin match with_tls with
    | Some (cert_file, key_file) ->
      TLS.server_context 
        ?protocol_version:ssl_protocol_version (cert_file, key_file)
      >>= fun ctx ->
      return (Some (object method context = ctx end))
      (* | None -> return (Lwt_ssl.plain (fst accepted)) *)
    | None -> return None
    end
    >>= fun tls_opt ->
    let caller_waiter = Light.create () in
    let self_stopper = Light.create () in
    let stop_on =
      match stop_on with
      | Some s -> s
      | None -> self_stopper in
    let rec accept_loop count =
      wrap_deferred_net (fun () -> 
        Lwt.(
          pick [
            begin
              Debug.(p@fun () -> f "[accept_loop %d] block on accept_n" count);
              Lwt_unix.accept_n socket accept_number
              >>= fun (accepted_list, potential_exn) ->
              Debug.(p@fun () -> f  "[accept_loop %d]  accept_n → %d connections" count (List.length accepted_list));
              begin match potential_exn with
              | Some Lwt.Canceled  ->
                Debug.(p@fun () -> f  "[accept_loop %d] Thread cancelled" count);
                return false
              | Some exn ->
                (handler (`Accept_exn exn)
                 >>= function 
                 | `Ok () -> return true
                 | `Error () -> return false)
              | None -> return true
              end
              >>= begin function
              | true ->
                return  (`Go (accepted_list))
              | false -> 
                Debug.(p@fun () -> f  "[accept_loop %d] stopped by on_error" count);
                return  `Stop
              end
            end;
            begin
              Debug.(p@fun () -> f  "[accept_loop %d] block on stop_on" count);
              Light.try_to_pass stop_on >>= fun _ -> 
              Debug.(p@fun () -> f  "[accept_loop %d] stopped by user" count);
              return  `Stop
            end;
            begin
              Debug.(p@fun () -> f  "[accept_loop %d] block on self_stopper" count);
              Light.try_to_pass self_stopper >>= fun _ -> 
              Debug.(p@fun () -> f  "[accept_loop %d] (re-)stopped by myself" count);
              return  `Stop
            end;
          ]))
      >>= function
      | `Go accepted_list ->
        accept_loop (count + 1) |> Lwt.ignore_result;
        let call_handler a =
          handler a >>< function
          | `Ok () -> return ()
          | `Error () ->
            Debug.(
              p @ fun () ->
                f "[handle_one %d] stopping everything (handler return)" count
            );
            Light.green self_stopper;
            Light.green caller_waiter;
            return ()
        in
        let handle_one accepted =
          begin match tls_opt with
          | Some tls ->
            TLS.accept (fst accepted) tls#context
            >>= fun ssl_socket ->
            return ssl_socket
          | None ->
            let socket = Lwt_ssl.plain (fst accepted) in
            return socket
          end
          >>< begin function
          | `Ok tls_socket ->
            (* TODO also give the (snd accepted) to the user *)
            call_handler (`Connection (Connection.of_tls_socket tls_socket))
          | `Error (`Net (`TLS (`Openssl_exn _ as openssl))) ->
            (* most likely here: unix.accept has succeeded but OpenSSL
              failed, so the client maybe between unix and SSL connection,
              by shutting the socket down we should unblock it. *)
            begin try
                Lwt_unix.shutdown (fst accepted) Lwt_unix.SHUTDOWN_ALL;
            with _ -> ()
            end;
            call_handler openssl
          end
        in
        Deferred_list.for_concurrent accepted_list ~f:handle_one 
        >>= fun ((_ : unit list), (_ : [ `no_errors_left ] list)) ->
        return ()
      | `Stop ->
        Debug.(p@fun () -> f  "[accept_loop %d on `Stop] stopping everything" count);
        Light.green self_stopper;
        Light.green caller_waiter;
        Lwt_unix.shutdown socket Lwt_unix.SHUTDOWN_ALL;
        return ()
    in
    accept_loop 0 |> Lwt.ignore_result;
    return caller_waiter

  let plain ?socket_options ?accept_number ?listen_on ?stop_on ~port handler =
    generic ?socket_options ?accept_number ?listen_on ?stop_on ~port (function
    | `Openssl_exn _ -> assert false
    | `Connection _ | `Accept_exn _ as arg -> handler arg)
    >>< function
    | `Ok o -> return o
    | `Error (`Net (`TLS _)) -> assert false
    | `Error (`Net #tcp_server_error as e) -> fail e

  let tls ?socket_options  ?accept_number
      ?ssl_protocol_version
      ~cert_key
      ?listen_on ?stop_on ~port handler =
    generic ?socket_options  ?accept_number
      ?ssl_protocol_version
      ~with_tls:cert_key
      ?listen_on ?stop_on ~port handler
end


#!/usr/bin/env ocaml

#use "topfind"
#thread
#require "pvem_lwt_unix,lwt.ssl"
#directory "_build/"
#load "_build/pvem_lwt_net.cma"

open Printf
open Pvem_lwt_unix
open Deferred_result
let say fmt = ksprintf (eprintf "%s\n%!") fmt

open Pvem_lwt_net

let fail_test e = fail (`Test_failed e)
let failf fmt = ksprintf fail_test fmt
let exn = Printexc.to_string

let fail_if_exn  f =
  wrap_deferred 
    ~on_exn:(fun e ->
      `Test_failed (sprintf "UNEXPECTED EXN: %s" (Printexc.to_string e)))
    f

let test_address () =
  begin
    Address.resolve_one "google.com"
    >>< function
    | `Ok one -> return ()
    | `Error (`Net (`No_host_entry s)) -> failf "No_host_entry: for %s" s
    | `Error (`Net (`Exn e)) -> failf "Net: Exn %s" (exn e)
  end
  >>= fun () ->
  begin
    Address.resolve_one "alkdsjfalkdsjfldsakjfdlsaj.mondet.org"
    >>< function
    | `Ok one -> failf "Found host: %s" (Address.to_string one)
    | `Error (`Net (`No_host_entry s)) -> return ()
    | `Error (`Net (`Exn e)) ->
      failf "Net: Exn %s (resolve_one big unexistend)" (exn e)
  end
  >>= fun () ->
  begin
    Address.resolve_all "google.com"
    >>< function
    | `Ok [] -> failf "resolve_all → [] for google.com"
    | `Ok more -> return ()
    | `Error (`Net (`Exn e)) -> failf "Net: Exn %s" (exn e)
  end

let test_plain_client () =
  begin
    Address.resolve_one "google.com"
    >>= fun google ->
    Client.connect_tcp ~address:google ~port:80 ()
    >>= fun connection ->
    say "Plain Client Connected !";
    Connection.shutdown connection
  end >>< function
  | `Ok () -> return ()
  | `Error (`Net _ as e) -> failf "TCP-client: %s" (Error.to_string e)

let test_tls_nonverifing_anonymous_client () =
  begin
    Address.resolve_one "google.com"
    >>= fun google ->
    Client.connect_unsecure_tls ~address:google ~port:443 `Anonymous
    >>= fun connection ->
    say "HTTPS Client Connected !";
    Connection.shutdown connection
  end >>< function
  | `Ok () -> return ()
  | `Error (`Net _ as e) -> failf "tls-nonverif %s" (Error.to_string e)

let test_tls_verifing_anonymous_client hostname  =
  begin
    Address.resolve_one hostname
    >>= fun google ->
    Client.connect_basic_tls
      ~verification_locations:(`Directory "/etc/ssl/certs/")
      ~address:google ~port:443
      `Anonymous
    >>= fun connection ->
    say "HTTPS Client Connected !";
    Connection.shutdown connection
  end >>< function
  | `Ok () -> return ()
  | `Error (`Net _ as e) -> failf "%s" (Error.to_string e)

let test_plain_server ~stops ~port =
  begin
    let stop_on, stop_fun, connection_return =
      match stops with
      | `With_stop_on ->
        let stop_on = Light.create () in
        (Some stop_on, (fun () -> Light.green stop_on), return)
      | `By_failing_in_handler ->
        (None, (fun () -> ()), fail)
    in
    Server.plain
      ~listen_on:`Any
      ?stop_on
      ~port
      (function
      | `Connection connection ->
        say "New connection !";
        connection_return ()
      | `Accept_exn e ->
        say "There was an accept exception: %s" (Printexc.to_string e);
        fail ()
      )
    >>= fun wait_for_end_of_server ->
    Address.resolve_one "localhost"
    >>= fun localhost ->
    Client.connect_tcp ~address:localhost ~port ()
    >>= fun connection ->
    say "Plain Client Connected to localhost:%d!" port;
    Connection.shutdown connection
    >>= fun () ->
    Deferred_list.for_concurrent ~f:(fun e -> e) [
      begin 
        fail_if_exn (fun () -> Lwt_unix.sleep 1.)
        >>= fun () ->
        say "Stoppping server: %f" (Unix.gettimeofday ());
        stop_fun ();
        return ()
      end;
      begin
        say "Waiting for server to end: %f" (Unix.gettimeofday ());
        Light.try_to_pass wait_for_end_of_server >>= fun () ->
        say "Server ended: %f" (Unix.gettimeofday ());
        fail_if_exn Lwt.(fun () -> Lwt_unix.sleep 1.)
      end;
    ]
    >>= fun (_, errors) ->
    begin match errors with
    | [] -> return ()
    | some :: _ -> fail some
    end
  end >>< function
  | `Ok () -> return ()
  | `Error (`Test_failed _ as e) -> fail e
  | `Error (`Net _ as e) -> failf "%s" (Error.to_string e)

let test_tls_server ~port ~push_to =
  begin
    if push_to = `Eleven then Inspection.debug := false;
    let cert, key =
      Filename.temp_file "pvem_lwt_net_test_" "_cert.pem",
      Filename.temp_file "pvem_lwt_net_test_" "_keyunsec.pem" in
    ksprintf System.Shell.do_or_fail
      "openssl req -x509 -newkey rsa:2048 -keyout %s -out %s -days 10 -nodes -subj \"/CN=test_pvem\" 2> /dev/null"
      key cert
    >>= fun () ->
    say "Created %s and %s" cert key;
    let connections = ref 0 in
    let max_connections = match push_to with `Ten -> 3 | `Eleven -> 400 in
    (* The actual limit will depend the max number of file descriptors,
       which can be set (within global limits) with `ulimit -n <nb>` *)
    let stop_on = Light.create () in
    Server.tls
      ~cert_key:(cert, key) ~listen_on:`Any ~port
      ~accept_number:20 ~stop_on
      (function
      | `Connection connection ->
        incr connections;
        if push_to = `Ten then say "TLS connection n° %d" !connections;
        let input = Connection.in_channel connection in
        let output = Connection.out_channel connection in
        begin
          begin
            fail_if_exn (fun () -> Lwt_io.read_line input)
            >>= fun line ->
            if push_to = `Ten then say "Client said %S"  line;
            fail_if_exn (fun () -> Lwt_io.fprintf output "Hello back\n")
          end
          >>< function
          | `Ok () ->  return ()
          | `Error (`Test_failed s) -> say "Read error : %s" s; return ()
        end
        >>= fun () ->
        begin match !connections >= max_connections with
        | true -> fail ()
        | false -> return ()
        end
      | `Accept_exn e ->
        say "There was an accept exception: %s" (Printexc.to_string e);
        fail ()
      | `Openssl_exn (e, msg) ->
        say "There was an openssl exception: %s (%s)" 
          (Printexc.to_string e) msg;
        fail ()
      )
    >>= fun wait_for_end_of_server ->
    Address.resolve_one "localhost"
    >>= fun localhost ->
    let client n =
      System.with_timeout 3. (fun () ->
        if push_to = `Ten then say "Client connecting to localhost:%d" port;
        Client.connect_unsecure_tls ~address:localhost ~port `Anonymous
        >>= fun connection ->
        if push_to = `Ten then say "UnSSL Client n° %d Connected !" n;
        let inp = Connection.in_channel connection in
        let out = Connection.out_channel connection in
        fail_if_exn (fun () -> Lwt_io.fprintf out "Hello, I am %d\n" n)
        >>= fun () ->
        fail_if_exn (fun () -> Lwt_io.read_line inp)
        >>= fun line ->
        if push_to = `Ten then say "Server said %S" line;
        Connection.shutdown connection)
    in
    let list_of_ints = Array.(init max_connections (fun e -> e) |> to_list) in
    begin match push_to with
    | `Ten ->
      Deferred_list.while_sequential list_of_ints client
      >>= fun _ ->
      return ()
    | `Eleven ->
      Deferred_list.for_concurrent list_of_ints client
      >>= fun (units, errors) ->
      say "%d clients failed" (List.length errors);
      Deferred_list.while_sequential [1;2] client
      >>= fun _ ->
      say "2 more clients went through";
      Light.green stop_on;
      return ()
    end
    >>= fun () ->
    say "Waiting for server to end";
    (* System.with_timeout 4. (fun () -> *) 
    Light.try_to_pass wait_for_end_of_server
    (* ) *)
    >>= fun () ->
    ksprintf System.Shell.do_or_fail "rm -f %s %s" key cert
  end >>< function
  | `Ok () -> return ()
  | `Error (`Test_failed _ as e) -> fail e
  | `Error (`Net _ as e) -> failf "%s" (Error.to_string e)
  | `Error (`shell (cmd, _)) -> failf "Failure of shell command:\n    %s" cmd
  | `Error (`system_exn e) -> failf "system_exn: %s" (exn e)
  | `Error (`timeout e) -> failf "time-out: %f" e

let () =
  if Array.length Sys.argv = 1 then exit 0; (* ← testing compilation only *)
  Inspection.debug := true;
  Lwt_main.run begin
    let section fmt = ksprintf (say "======= %s =======") fmt in
    Transport_layer_security.init ();
    say "Test starts.";
    section "test_address";
    test_address ()
    >>= fun () ->
    section "test_plain_client";
    test_plain_client ()
    >>= fun () ->
    section "test_tls_nonverifing_anonymous_client";
    test_tls_nonverifing_anonymous_client ()
    >>= fun () ->
    section "test_tls_verifing_anonymous_client";
    test_tls_verifing_anonymous_client "google.com"
    >>= fun () ->
    section "test_tls_verifing_anonymous_client www.google";
    test_tls_verifing_anonymous_client "www.google.com"
    >>= fun () ->
    section "test_tls_verifing_anonymous_client gencore.bio.nyu.edu";
    test_tls_verifing_anonymous_client "gencore.bio.nyu.edu"
    >>= fun () ->
    section "test_tls_verifing_anonymous_client that should fail";
    begin 
      test_tls_verifing_anonymous_client "godirepo.camlcity.org"
      >>< function
      | `Ok () -> failf "this test should fail because of wrong SSL cert"
      | `Error e -> return ()
    end
    >>= fun () ->
    section "test_plain_server stop_on";
    Deferred_list.pick_and_cancel [
      test_plain_server ~port:20000 ~stops:`With_stop_on;
      Lwt.(Lwt_unix.sleep 5. >>= fun () -> return (`Error (`Test_failed "TIMEOUT")));
    ]
    >>= fun () ->
    section "test_plain_server handler fails";
    Deferred_list.pick_and_cancel [
      test_plain_server ~port:20001 ~stops:`By_failing_in_handler;
      Lwt.(Lwt_unix.sleep 5. >>= fun () -> return (`Error (`Test_failed "TIMEOUT")));
    ]
    >>= fun () ->
    section "test_tls_server → Ten";
    test_tls_server ~port:30000 ~push_to:`Ten
    >>= fun () ->
    section "test_tls_server → Eleven";
    test_tls_server ~port:30001 ~push_to:`Eleven
  end
  |> function
  | `Ok () -> 
    say "TEST SUCCEEDED!";
    exit 0
  | `Error e ->
    say "TEST: Error:\n%s\n"
      begin match e with
      | `Test_failed s -> sprintf "Test_failed: %s" s
      | `Failure e -> sprintf "Failure: %s" (exn e)
      end;
    exit 1


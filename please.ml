#! /usr/bin/env ocaml
 open Printf
 module List = ListLabels
 let say fmt = ksprintf (printf "Please-> %s\n%!") fmt
 let cmdf fmt =
   ksprintf (fun s -> ignore (Sys.command s)) fmt
 let chain l =
   List.iter l ~f:(fun cmd ->
       printf "! %s\n%!" cmd;
       if  Sys.command cmd = 0
       then ()
       else ksprintf failwith "%S failed." cmd
     )
 let args = Array.to_list Sys.argv
 let in_build_directory f =
   cmdf "mkdir -p _build/";
   Sys.chdir "_build/";
   begin try
     f ();
   with
     e ->
     Sys.chdir "../";
     raise e
   end;
   Sys.chdir "../";
   ()


let build () =
in_build_directory (fun () ->
chain 
[
    "cp ../pvem_lwt_net.ml .";
    "ocamlfind ocamlc -package pvem,pvem_lwt_unix,unix,threads,lwt.preemptive,lwt.unix,ssl,unix,threads,lwt.preemptive,lwt.unix -thread -c pvem_lwt_net.ml -o pvem_lwt_net.cmo";
    "ocamlfind ocamlopt -package pvem,pvem_lwt_unix,unix,threads,lwt.preemptive,lwt.unix,ssl,unix,threads,lwt.preemptive,lwt.unix -thread -c pvem_lwt_net.ml  -annot -bin-annot -o pvem_lwt_net.cmx";
    "ocamlc pvem_lwt_net.cmo -a -o pvem_lwt_net.cma";
    "ocamlopt pvem_lwt_net.cmx -a -o pvem_lwt_net.cmxa";
    "ocamlopt pvem_lwt_net.cmxa pvem_lwt_net.a -shared -o pvem_lwt_net.cmxs";

]

)

let install () =
    in_build_directory (fun () ->
        chain [
          "ocamlfind install pvem_lwt_net ../META pvem_lwt_net.cmx pvem_lwt_net.cmo pvem_lwt_net.cma pvem_lwt_net.cmi pvem_lwt_net.cmxa pvem_lwt_net.cmxs pvem_lwt_net.a pvem_lwt_net.o"
        ])


let uninstall () =
    chain [
      "ocamlfind remove pvem_lwt_net"
    ]


let merlinize () =
    chain [
      "echo 'S .' > .merlin";
      "echo 'B _build' >> .merlin";
      "echo 'PKG pvem' >> .merlin";
"echo 'PKG pvem_lwt_unix' >> .merlin";
"echo 'PKG unix' >> .merlin";
"echo 'PKG threads' >> .merlin";
"echo 'PKG lwt.preemptive' >> .merlin";
"echo 'PKG lwt.unix' >> .merlin";
"echo 'PKG ssl' >> .merlin";
"echo 'PKG unix' >> .merlin";
"echo 'PKG threads' >> .merlin";
"echo 'PKG lwt.preemptive' >> .merlin";
"echo 'PKG lwt.unix' >> .merlin";
     ]


let build_doc () =
    in_build_directory (fun () ->
        chain [
          "mkdir -p doc";
                         sprintf "ocamlfind ocamldoc -package pvem,pvem_lwt_unix,unix,threads,lwt.preemptive,lwt.unix,ssl,unix,threads,lwt.preemptive,lwt.unix -thread -charset UTF-8 -keep-code -colorize-code -html pvem_lwt_net.ml -d doc/";
        ])


let name = "pvem_lwt_net"

let () = begin
match args with
| _ :: "build" :: [] ->(
say "Building";
build ();
say "Done."
)
| _ :: "build_doc" :: [] ->(
say "Building Documentation";
build_doc ();
say "Done."
)
| _ :: "install" :: [] ->(
say "Installing";
install ();
say "Done."
)
| _ :: "uninstall" :: [] ->(
say "Uninstalling";
uninstall ();
say "Done."
)
| _ :: "merlinize" :: [] ->(
say "Updating `.merlin` file";
merlinize ();
say "Done."
)
| _ :: "clean" :: [] ->(
say "Cleaning";
cmdf "rm -fr _build";
say "Done."
)
| _ ->(
say "usage: ocaml %s [build|install|uninstall|clean|build_doc|melinize]" Sys.argv.(0)
)

end


